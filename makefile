EXE = comissao
CC = gcc
CFLAGS = -std=c99 -Wall
OBJ = main.o
# all: main.c
# 	gcc main.c -std=c99 -Wall
# 	./a.out < input0

# 1: main.c
# 	gcc main.c -std=c99 -Wall -DDEBUG 
# 	./a.out < input/input4 | less

all: $(OBJ)
	$(CC) $(CFLAGS) main.c -o $(EXE)
	rm -f $(OBJ)

debug: $(OBJ)
	$(CC) $(CFLAGS) -DDEBUG main.c -o $(EXE)

main.o: main.c
	$(CC) $(CFLAGS) -c main.c

clean: 
	rm -f $(EXE)

compact:
	tar -cvzf $(EXE).tar.gz *.c *.h makefile