/* Otimizacao - T2: Comissao Representativa (2024_01)
 * Alunos: 
 *  - Luan Carlos Maia Cruz
 *  - Matheus de Souza Feitosa  
 *
 * Standard: C99
 */

#include <stdio.h>  /* fgets, sscanf, printf */
#include <string.h> /* strtok, memset */
#include <stdlib.h> /* atoi */
#include <limits.h> /* INT_MAX */

#define BUFFER_SIZE 1024

/* Biblioteca de input/output
 *  - Funcoes basicas para ler e imprimir o formato de entrada do trabalho 
 */

int g_l, g_n, g_opt;

void input_init(int l, int n, int *S, int *C, int *Sc) {

    S[0] = l;
    memset(&S[1], 0, sizeof (int) * l);

    C[0] = n;
    memset(&C[1], 0, sizeof (int) * n);

    memset(&Sc[0], 0, sizeof (int) * (n+1) * (l+1));
}

void input_print(int l, int n, const int *S, const int *C, const int *Sc) {
    int i, j;

    printf("S: ");
    for (i = 0; i <= l; ++i)
        printf("%d ", S[i]);
    putchar('\n');

    printf("C: ");
    for (i = 0; i <= n; ++i)
        printf("%d ", C[i]);
    putchar('\n');

    printf("Sc:\n");
    for (i = 0; i <= n; ++i, putchar('\n'))
        for (j = 0; j <= l; ++j)
            printf("%d ", Sc[i*(l+1)+j]);
}

/* --- --- */

/* Biblioteca de  Conjutos
 *  - Define uma estruturas de dados que se comporta como um conjunto matematico
 */

void set_copy(int *S0, int *S1, int l) { /* S0 := S1 */
    for (int i = 0; i <= l; ++i)
        S0[i] = S1[i];
}

void set_print(int *S, int l) {
    if (!S)
        return;

    if (S[0] == -1) {
        puts("-1");
        return;
    }

    for (int i = 0; i <= l; ++i)
        printf("%2d ", S[i]);
    putchar('\n');
}

int set_size(int *S, int l) {
    int i, count;

    if (!S)
        return 0;

    if (S[0] == -1)
        return INT_MAX;

    for (i = 1, count = 0; i <= l; ++i)
        count += S[i];

    return count;
}

/* --- --- */


int cut_viability(int l, int n, const int *C, const int *Sc) {
    int S[l+1];
    int i, j;

    S[0] = l;
    memset(&S[1], 0, sizeof (int) * l);

    for (i = 1; i <= n; ++i)    /* Para todos os Candidatos */
        for(j = 1; j <= l; ++j) 
                S[j] |=  C[i] && Sc[i*g_n+j];

    #if DEBUG2
        printf("cut_viability: ");

        for (i = 0; i <= l; ++i)
            printf("%d ", S[i]);
    #endif
    
    for(i = 1; i <= l; ++i) {
        if (!S[i])
            return -1;
    }

    return 0;
}

/* Lendo Fora do Stack Pointer (acredito que nao tenha probemas nesse caso) */

/* branch_and_bound_bt (binary tree)
 * Retorna o conjunto minimo em r 
 */

/*
 *                              (|||)
 *              (0||)                           (1||)
 *      (00|)           (01|)           (10|)           (11|)
 * (000)     (001) (010)     (011) (100)     (101) (110)     (111)
 * 
 */

int *branch_and_bound_bt(int l, int *Sc, int h, int *C, int *r) { 
    if (h == g_n) { 
        #if DEBUG
            printf("%d | ", h);
            printf("leaf: ( ");
            for (int i = 1; i <= h; ++i)
                printf("%d ", C[i]);
            printf(") | %2d \n", cut_viability(l, g_n, C, Sc));
        #endif

        return (!cut_viability(l, g_n, C, Sc) ? C : NULL);
    }

    #if DEBUG
        printf("%d | ", h);
        printf("branch: ( ");
        for (int i = 1; i <= h; ++i)
            printf("%d ", C[i]);
        printf(") | %2d \n", cut_viability(l, h, C, Sc));
    #endif

    if (!cut_viability(l, h, C, Sc)) { /* Corte por Otimalidade Professor */
        for (int i = h+1; i <= g_n; ++i)
            C[i] = 0;
        #if DEBUG 
            printf("Otimo na subarvore:");
            set_print(C, g_n);
        #endif

        set_copy(r, C, g_n);
        
        return C;
    }

    int *left, *right;
    ++h; 

    if(h > g_opt) /* Corte por Otimalidade Autores */
        return NULL;


    C[h] = 0;
    left = branch_and_bound_bt(l, Sc, h, C, r); // Branch Esquerda 
    int set_left[g_n];

    if (!left) 
        set_left[0] = -1;
    else set_copy(set_left, left, g_n);    

    C[h] = 1;
    right = branch_and_bound_bt(l, Sc, h, C, r); // Branch Direita 
    int set_right[g_n];

    if (!right) 
        set_right[0] = -1;
    else set_copy(set_right, right, g_n);
    

    #if DEBUG
        puts("------------");
        printf("%d\n", h-1);
        set_print(set_left, g_n);
        set_print(set_right, g_n);

        int *d_aux = (set_size(set_left, g_n) > set_size(set_right, g_n) ? set_right : set_left);
        printf("Set Min:");
        set_print(d_aux, g_n);
        //printf("%p\n", d_aux);
        puts("------------");

        set_copy(r, d_aux, g_n);

        int d_size = set_size(d_aux, g_n);
        if(d_size < g_opt)
            g_opt = d_size;
        printf("g_opt: %d\n", g_opt);

        return d_aux;
    #endif

    int *set_min = (set_size(set_left, g_n) > set_size(set_right, g_n) ? set_right : set_left);

    set_copy(r, set_min, g_n);
    for (int i = 0; i <= g_n; ++i)
        r[i] = set_min[i];

    int size = set_size(set_min, g_n);
    // printf("g_opt: %d\n", g_opt);
    if(size < g_opt)
        g_opt = size;
    return set_min;
}   

/* --- --- */
/*
    Opções:
    -f - Desligar cortes por viabilidade
    -o - Desligar cortes por otimalidade
    -a - Usar função dada pelo professor
*/

int main(int argc, char *argv[]) {

    char INPUT_BUFFER[BUFFER_SIZE];
    fgets(INPUT_BUFFER, BUFFER_SIZE, stdin);
    sscanf(INPUT_BUFFER, "%d %d\n", &g_l, &g_n);

    int S[g_l+1], C[g_n+1]; 
    int Sc[g_n+1][g_l+1];
    g_opt = INT_MAX;
    input_init(g_l, g_n, S, C, Sc[0]);
    
    
    for (int i = 1; i <= g_n; ++i)
    {
        scanf("%d", &Sc[i][0]);

        for(int j = 1; j <= Sc[i][0]; ++j)
        {
            int aux;
            scanf("%d", &aux);
            Sc[i][aux] = 1;
        }
    }

    /* --- ---  */

    #if DEBUG
        input_print(g_l, g_n, S, C, Sc[0]); putchar('\n');
    #endif

    /* --- --- */

    // printf("Entrada:");
    // printf("\n%d %d", g_l, g_n);
    // for(int i = 1; i <= g_n; ++i) {
    //     printf("\n%d", Sc[i][0]);
    //     for(int j = 1; j <= g_l; ++j) {
    //         if(Sc[i][j])
    //             printf(" %d", j);
    //     }
    // }

    int r[g_n];
    branch_and_bound_bt(g_l, Sc[0], 0, C, r); /* NAO ESTA ALOCADO */

    #if DEBUG
        printf("\nConjunto Minimo: "); set_print(r, g_n);
    #endif

    // printf("\n\nSaída:\n");
    
    if (r[0] != -1) { /* Invalido */
        for (int i = 1; i <= g_n; ++i) { 
            if (r[i]) {
                printf("%d ", i);
            }
        }
        putchar('\n');
    } else puts("Inviavel");

    return 0;
}

